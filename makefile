OUT = klient_server
CC = gcc
SOURCES = src/klient.c src/server.c
OBJECTS = $(SOURCES:.c=.o)
LIBS = 
INCLUDES = 
CFLAGS = -g -Wall -Werror
LDFLAGS = 

all:	$(OBJECTS)
	$(CC) $(LDFLAGS) klient.o -o klient
	$(CC) $(LDFLAGS) server.o -o server

$(OBJECTS):	$(SOURCES)
	$(CC) $(CFLAGS) -c $(SOURCES)

clean:	
	rm -fv klient.o server.o  klient server




