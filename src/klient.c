#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/epoll.h>
#include <errno.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <signal.h>

#define check_error(condition, msg, retval) \
    if (condition){\
        perror(msg);\
        return(retval);\
    }

const size_t MAX_BUFF = 1024;   // maksymalny rozmiar bufora
const char IP[] = "127.0.0.1";
const int PORT = 5555;
int conneted = 1; // Zmienna do przerywania połączenia

size_t getlinia(char s[], int lim);
void catchSIGINT(int unused);
void prepareSignals(void);

int main (int argc, char ** argv)
{
    int result = -1;
    int fd = -1;
    char buff[MAX_BUFF];
    memset(buff, 0, MAX_BUFF * sizeof(char));
    ssize_t char_read = -1;

    prepareSignals();
    // Tworzymy socketa
    fd = socket(AF_INET, SOCK_STREAM, 0);
        check_error(fd == -1, "socket", 1);
    printf("Stworzono socket %d.\n", fd);

    // Tworzenie adresu połączenia
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    inet_aton(IP, (struct in_addr*)&serv_addr.sin_addr.s_addr);

    result = connect(fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
        check_error(result == -1, "connect", 3);
    printf("Połączono.\n");

    size_t count;
    size_t msg_len = -1;
    while(conneted){
        printf("> ");
        count = getlinia(buff, MAX_BUFF);

        char_read = write(fd, &count, sizeof(count));
            check_error(char_read == -1, "write", 4);

        char_read = write(fd, buff, count);
            check_error(char_read == -1, "write", 4);

        char_read = read(fd, &msg_len, sizeof(msg_len));
          check_error(char_read == -1, "read", 4);

        char_read = read(fd, (void*)buff, msg_len);
          check_error(char_read == -1, "read", 4);

        buff[msg_len] = '\0';
        printf("Wiadomość zwrotna: %s (długość: %lu)\n", buff, msg_len);
    }

    result = close(fd);
        check_error(result == -1, "close", 2);
    printf("Zamknięto socket %d.\n", fd);
    return 0;
}

// getline - pobiera wiersz do s, zwraca długość
size_t getlinia(char s[], int lim)
{
    int c;
    size_t i = 0;
    while(--lim > 0 && (c = getchar()) != EOF && c != '\n')
        s[i++] = c;
    s[i] = '\0';
    return i;
}

void prepareSignals(void)
{
  sigset_t emptySet;
  sigemptyset(&emptySet);
  struct sigaction act;
  act.sa_mask = emptySet;
  act.sa_flags = 0;
  act.sa_handler = &catchSIGINT;
  sigaction(SIGINT, &act, 0);
}

void catchSIGINT(int unused)
{
  conneted = 0;
  printf("\n");
}
