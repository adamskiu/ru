#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#define check_error(condition, msg, retval) \
    if (condition){\
        perror(msg);\
        return(retval);\
    }

struct user_info{
  int fd;
  char *username;
};

const size_t MAX_BUFF = 1024;   // maksymalny rozmiar bufora
const size_t MAX_CLI = 5;       // maksymalna liczba klientów
const int PORT = 5555;
int server_active = 1;



int read_up_to_dot(const char* s, int limit);

int main (int argc, char ** argv)
{
    int result = 0;
    int i = 0;

    int srv_fd = -1;    // Server file descriptor
    int cli_fd = -1;    // client file descriptor
    int epoll_fd = -1;  // epoll file descriptor

    struct sockaddr_in srv_addr;
    struct epoll_event e;
    char buff[MAX_BUFF];
    char ret_buff[MAX_BUFF];
    char username[MAX_BUFF];
    char message[MAX_BUFF];
    ssize_t read_cnt = 0;
    size_t msg_len = -1;
    size_t ret_msg_len = -1;
    size_t msg_to_len = -1;
    struct user_info active_users[MAX_CLI];
    for (i = 0; i < MAX_CLI; i++){
      active_users[i].fd = 0;
      active_users[i].username = 0;
    }


    // Zeruj pamięć
    memset(&srv_addr, 0, sizeof(srv_addr));
    memset(&buff, 0, MAX_BUFF * sizeof(char));
    memset(&message, 0, MAX_BUFF * sizeof(char));
    memset(&username, 0, MAX_BUFF * sizeof(char));
    memset(&e, 0, sizeof(e));
    memset(&ret_buff, 0, MAX_BUFF * sizeof(char));

    // Stwórz socket, ale jeszcze należy do niego przypisać adres
    srv_fd = socket(AF_INET, SOCK_STREAM, 0);   // IPv4, komunikacja połączeniowa
        check_error(srv_fd == -1, "socket", 1);

    srv_addr.sin_family = AF_INET;          // IPv4
    srv_addr.sin_port = htons(PORT);        // port
    srv_addr.sin_addr.s_addr = INADDR_ANY;  // przyjmujemy połączenia na każdym interfejsie

    // Przypisz adres do socketa
    result = bind(srv_fd, (struct sockaddr*) &srv_addr, sizeof(srv_addr));
        check_error(result == -1, "bind", 2);

    // Słuchaj na danym deskryptorze
    result = listen(srv_fd, MAX_CLI);
        check_error(result == -1, "listen", 3);

    // Tworzymy epolla
    epoll_fd = epoll_create(MAX_CLI+1);
        check_error(epoll_fd == -1, "epoll_create", 4);

    e.events = EPOLLIN;
    e.data.fd = srv_fd;

    // Dodajemy do niego gniazdo servera
    result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, srv_fd, &e);
        check_error(result == -1, "epol_ctl", 5);

    //++++++++++++++++++++++++++++++++++++++++++++++++
    while (server_active) {
      // Czekamy na I/O na gnieździe, zwraca liczbę gotowych deskryptorów
        i = epoll_wait(epoll_fd, &e, 1, -1);
                check_error(i == -1, "epoll_wait", 6);
        if (i > 0) {
            if (e.data.fd == srv_fd) {    // dostaliśmy nowego klienta
                printf("client accepted\n");

                cli_fd = accept(srv_fd, 0, 0);
                    check_error(cli_fd == -1, "accept", 7);

                e.events = EPOLLIN;
                e.data.fd = cli_fd;

                result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, cli_fd, &e);
                    check_error(result == -1, "epol_ctl", 5);
            }
            else {  //  e.data != srv.fd
                cli_fd = e.data.fd;

                //Tutaj czytaj długość wiadomości którą otrzymamy
                read_cnt = read(cli_fd, (void *) &msg_len, sizeof(msg_len));
                    check_error(read_cnt == -1, "read", 8);

                if (read_cnt == 0) {    // jeśli klient się rozłączył
                    printf("client %d gone\n", cli_fd);

                    result = epoll_ctl(epoll_fd, EPOLL_CTL_DEL, cli_fd, 0);
                        check_error(result == -1, "epol_ctl", 5);

                    result = close(cli_fd);
                        check_error(result == -1, "close", 10);
                }
                else {                // Jeśli dostaliśmy wiadomość
                    read_cnt = read(cli_fd, (void *)buff, msg_len * sizeof(char));
                      check_error(read_cnt == -1, "read", 8);
                    buff[read_cnt] = 0;

                    strcpy(ret_buff, "0.OK");
                    ret_msg_len = strlen(ret_buff);
                    i = 0;
                    while (active_users[i].fd != 0) {
                      printf("%d %s\n",active_users[i].fd ,active_users[i].username );
                      i++;
                    }
                    switch (buff[0]){
                      case '1':{ // ERROR
                        printf("Jakiś błąd.\n");
                        break;
                      }
                      case '2':{ // LogIn
                        strcpy(ret_buff, "1.0.Nieprawidłowy format wiadomości.");
                        ret_msg_len = strlen(ret_buff);
                        int offset = 2;
                        if (buff[1] == '.'){
                          char *username = (char*) malloc(strlen(&buff[offset]) * sizeof(char) +1 );
                          strcpy(username, &buff[offset]);
                          printf("Klient chce się zalogować pod pseudonimem %s \n", username );
                          i = 0;
                          while (active_users[i].fd != 0){
                            i++;
                          }
                          if (i == MAX_CLI){ // nie można podłączyć więcej kleintów
                            strcpy(ret_buff, "1.1.Nie można się zalogować, przekroczono maksymalną liczbę klientów.");
                            ret_msg_len = strlen(ret_buff);
                          }
                          else{
                            int j;
                            for(j = 0; j < MAX_CLI; j++){
                              if (active_users[j].fd == 0) continue;  // potencjalne zadanie optymalizacyjne
                              if (strcmp(active_users[j].username, username) == 0){
                                strcpy(ret_buff, "1.2.Nie można się zalogować, podany nick już istnieje.");
                                ret_msg_len = strlen(ret_buff);
                                break;
                              }
                            }
                            if (j == MAX_CLI){  // Nie ma takiego usera
                              active_users[i].fd = cli_fd;
                              active_users[i].username = username;
                              strcpy(ret_buff, "0.Zalogowano.");
                              ret_msg_len = strlen(ret_buff);
                            }
                          }
                        }
                      break;
                      }

                      case '3':{  // MessageTo

                        // Przydałoby się sprawdzić czy klient jest zalogowany

                        strcpy(ret_buff, "1.0.Nieprawidłowy format wiadomości.");
                        ret_msg_len = strlen(ret_buff);
                        if (buff[1] == '.'){
                          size_t nick_len = 0;    // może gdzieś sprawić problem ze scopem
                          int offset = 2; // indeks za kropką
                          nick_len = read_up_to_dot(&buff[offset], msg_len - offset);
                          if (nick_len != 0){ // wyłuskaj nick z wiadomości
                            strncpy(username, &buff[offset], nick_len);
                            offset = offset + nick_len;
                             if (buff[offset] == '.'){  // jeśli po nicku jest kropka
                               msg_to_len = read_up_to_dot(&buff[offset +1], msg_len - offset);
                               if (msg_to_len != 0){ // jeśli po kropce coś jest
                                 strncpy(message, &buff[offset + 1], msg_to_len);

                                 // udało się odczytać wszystko, tutaj wstawić coś zamiast OK
                                 // Ponadto wysłać wiadomość

                                 printf("MessageTo %s: %s\n",username, message );
                                 strcpy(ret_buff, "0.OK");
                                 ret_msg_len = strlen(ret_buff);
                               }
                             }
                          }
                         }
                        break;
                        }
                     case '4':{ // MessageFrom

                     }

                     case '6':{ // UserList
                       strcpy(ret_buff, "1.0.Nieprawidłowy format wiadomości.");
                       ret_msg_len = strlen(ret_buff);
                       if (buff[1] == '.'){
                         if (active_users[0].fd == 0){  // jeśli nie ma zalogowanych
                           strcpy(ret_buff, "1.3 Brak zalogowanych użytkowników.");
                           ret_msg_len = strlen(ret_buff);
                         }
                         else{  // są zalogowani
                           i = 0;
                           ret_buff[0] = '7';
                           int offset = 1;
                           while (active_users[i].fd != 0 ){
                             ret_buff[offset] = '.';
                             offset++;
                             strcpy(&ret_buff[offset], active_users[i].username);
                             offset = offset + strlen(active_users[i].username);
                             printf("offset = %d\n", offset );
                             i++;
                           }
                           ret_buff[offset] = '\0';
                         }
                       }

                       break;
                     }
                    }

                    printf("Wiadomość od %d: \'%s\' (długość: %lu)\n" ,cli_fd, buff, msg_len); // wyświetlamy wiadomość

                    result = write(cli_fd, &ret_msg_len, sizeof(ret_msg_len));
                        check_error(result == -1, "write", 9);

                    result = write(cli_fd, (void *) ret_buff, ret_msg_len);
                        check_error(result == -1, "write", 9);
                }
            }
        }
        memset(&e, 0, sizeof(e));
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++

    result = close(srv_fd);
        check_error(result == -1, "close", 10);
    result = close(epoll_fd);
        check_error(result == -1, "close", 10);
    return 0;
}

int read_up_to_dot(const char* s, int limit){
  int i = 0;
  if (s !=  NULL){
    int c = s[0];
    while(limit-- > 0 && (c = s[i]) != '.' && c != '\0')
      i++;
  }
  return i;
}
